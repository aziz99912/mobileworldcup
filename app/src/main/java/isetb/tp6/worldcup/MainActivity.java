package isetb.tp6.worldcup;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;

public class MainActivity extends AppCompatActivity implements View.OnClickListener {
    Button home,match,profil,tickets;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);


        home=findViewById(R.id.home);
        match=findViewById(R.id.match);
        profil=findViewById(R.id.profil);
        tickets=findViewById(R.id.tickets);

        home.setOnClickListener(this);
        match.setOnClickListener(this);
        profil.setOnClickListener(this);
        tickets.setOnClickListener(this);

    }

    @Override
    public void onClick(View v) {

        Intent i;
        switch (v.getId()){
            case R.id.home:
                i=new Intent(this,homeactivity.class);
                startActivity(i);
                break;
            case R.id.match:
                i=new Intent(this,MatchActivity.class);
                startActivity(i);
                break;
            case R.id.profil:
                i=new Intent(this, ProfilActivity.class);
                startActivity(i);
                break;
            case R.id.tickets:
                i=new Intent(this, TicketsActivity.class);
                startActivity(i);
                break;
            default: break;
        }
    }
}