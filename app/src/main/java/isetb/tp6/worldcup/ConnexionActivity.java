package isetb.tp6.worldcup;

import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;

public class ConnexionActivity extends AppCompatActivity {
    //private String Login ="Fatnassi";
    //  private  String  Password ="1234";

    EditText t1,t2 ;
    Button b1 ,b2;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_login);
        t1=findViewById(R.id.editpassworddd);
        t2=findViewById(R.id.editpassworddd);
        b1=findViewById(R.id.buttonlogin);
        b2=findViewById(R.id.buttonrr);
        b1.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                String log=t1.getText().toString();
                String pw=t2.getText().toString();
                if (log.equals("")||pw.equals(""))
                    Toast.makeText(ConnexionActivity.this,"champs vide" , Toast.LENGTH_SHORT).show();
                else{
                    SharedPreferences sp=getSharedPreferences("myinfo",MODE_PRIVATE);
                    String login=sp.getString("L","");
                    String password=sp.getString("P","");
                    if(!log.equals(login) || !pw.equals(password))
                        Toast.makeText( ConnexionActivity.this, "login ou mot de passe inccorect",Toast.LENGTH_SHORT).show();
               else{
                   Intent i=new Intent( ConnexionActivity.this,MainActivity.class);
                   i.putExtra("log", login);
                   startActivity(i);
                    }

                }

            }
        });
        b2.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent i2=new Intent (ConnexionActivity.this,EnregistrementActivity.class);
                startActivity(i2);
            }
        });
    }
}
