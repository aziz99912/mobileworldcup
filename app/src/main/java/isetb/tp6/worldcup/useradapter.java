package isetb.tp6.worldcup;

import android.app.Activity;
import android.content.DialogInterface;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.appcompat.app.AlertDialog;

import java.util.ArrayList;

public class useradapter extends ArrayAdapter<User> {

    Activity context;
    private ArrayList<User> listuser;
    private DatabaseHelper db;

    public  useradapter(Activity context, ArrayList<User> listuser) {
        super(context, R.layout.user_item, listuser);
        this.context = context;
        this.listuser = listuser;

    }

    @NonNull
    @Override
    public View getView(int position, @Nullable View convertView, @NonNull ViewGroup parent) {
        View view = LayoutInflater.from(context).inflate(R.layout.user_item, null, true);
        TextView t1 = view.findViewById(R.id.text_id);
        TextView t2 = view.findViewById(R.id.text_name);
        TextView t3 = view.findViewById(R.id.text_phone);
        ImageView im1 = view.findViewById(R.id.edit_student);
        ImageView im2 = view.findViewById(R.id.delete_student);
        t1.setText(String.valueOf(listuser.get(position).getId()));

        t2.setText(listuser.get(position).getNom());
        t3.setText(listuser.get(position).getPhone());
        User u = listuser.get(position);
        im1.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                LayoutInflater inflater = LayoutInflater.from(context);
                View subView = inflater.inflate(R.layout.adduser, null);
                EditText n = subView.findViewById(R.id.edit_name);
                EditText p = subView.findViewById(R.id.edit_phone);
                n.setText(u.getNom());
                p.setText(u.getPhone());
                AlertDialog.Builder a = new AlertDialog.Builder(context);
                a.setTitle("Edit new Student");
                a.setView(subView);
                a.setPositiveButton("Add", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        String name = n.getText().toString();
                        String phone = p.getText().toString();
                        // Student s = new Student(name, phone);
                        db = new DatabaseHelper(context);
                        db.updateUser(new User(u.getId(), name, phone));
                        context.finish();
                        context.startActivity(context.getIntent());
                    }
                });
                a.setNegativeButton("Cancel", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        Toast.makeText(context, "task canceld ", Toast.LENGTH_SHORT).show();
                    }
                });
                a.show();

            }
        });
        return view;
    }


}
