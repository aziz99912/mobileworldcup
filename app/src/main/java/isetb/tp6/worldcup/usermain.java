package isetb.tp6.worldcup;

import android.content.DialogInterface;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.EditText;
import android.widget.ListView;
import android.widget.Toast;

import androidx.appcompat.app.AlertDialog;
import androidx.appcompat.app.AppCompatActivity;

import com.google.android.material.floatingactionbutton.FloatingActionButton;

import java.util.ArrayList;

public class usermain extends AppCompatActivity {
    private ListView l;
    private DatabaseHelper db;
    private useradapter adapter;
    private ArrayList<User> listUser;
    private FloatingActionButton btn_add;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        l = findViewById(R.id.list_view);
        db = new DatabaseHelper(this);
        int nbStudent = db.getUserCount();
        if (nbStudent == 0)
            Toast.makeText(this, "Empty Database", Toast.LENGTH_SHORT).show();
        else {
            listUser = db.getAllUser();
            adapter = new useradapter(this, listUser);
            l.setAdapter(adapter);
        }


        btn_add=findViewById(R.id.btn_add);
        btn_add.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                LayoutInflater inflater=LayoutInflater.from(usermain.this);
                View subView=inflater.inflate(R.layout.adduser,null);
                EditText n=subView.findViewById(R.id.edit_name);
                EditText p=subView.findViewById(R.id.edit_phone);
                AlertDialog.Builder a =new AlertDialog.Builder(usermain.this);
                a.setTitle("Add new Student");
                a.setView(subView);
                a.setPositiveButton("Add", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        String nom=n.getText().toString();
                        String phone=p.getText().toString();
                        User u=new User(nom,phone);
                        db.addUser(u);
                        finish();
                        startActivity(getIntent());

                    }
                });
                a.setNegativeButton("Cancel", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        Toast.makeText(usermain.this, "task canceld " ,Toast.LENGTH_SHORT).show();
                    }
                });
                a.show();

            }
        });
    }
}
