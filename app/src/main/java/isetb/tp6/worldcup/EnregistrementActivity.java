package isetb.tp6.worldcup;

import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.os.PersistableBundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;

public class EnregistrementActivity  extends AppCompatActivity {

    EditText e1 , e2 ,e3 , e4 ,e5 ;
    Button btn_enregistre ;



    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_inscription);
        e1=findViewById(R.id.editname);
        e2=findViewById(R.id.editemail);
        e3=findViewById(R.id.editnumber);
        e4=findViewById(R.id.editpassword);
         e5=findViewById(R.id.editpasswordpwd);

        btn_enregistre=findViewById(R.id.buttonRegistre);
        btn_enregistre.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                SharedPreferences sp=getSharedPreferences("myinfo",MODE_PRIVATE);
                SharedPreferences.Editor editor=sp.edit();
                String nom=e1.getText().toString();
                editor.putString("N",nom);
                String email=e2.getText().toString();
                editor.putString("L",email);
                String number=e3.getText().toString();
                editor.putString("E",number);
                String pws=e4.getText().toString();
                editor.putString("P",pws);
                String cpw=e5.getText().toString();
                if(cpw.equals(pws)){
                    editor.commit();
                    Intent intent =new Intent(EnregistrementActivity.this,MainActivity.class);
                    startActivity(intent);
                }

                else
                    Toast.makeText(EnregistrementActivity.this,"verifier votre password",Toast.LENGTH_LONG).show();

            }
        });
    }
}
