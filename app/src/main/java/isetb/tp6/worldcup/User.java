package isetb.tp6.worldcup;

public class User {

    private int id ;
    private String nom ;
    private String prenom ;
    private String email ;
    private int phone ;
    private String password ;


    public User(int anInt, String string, String cString) {
    }

    public User(int id, String nom) {
        this.id = id;
        this.nom = nom;
    }

    public User(String id, String phone) {
        this.id = id;
        this.phone = phone;
    }

    public User(int id, String nom, String prenom, String email, int phone, String password) {
        this.id = id;
        this.nom = nom;
        this.prenom = prenom;
        this.email = email;
        this.phone = phone;
        this.password = password;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getNom() {
        return nom;
    }

    public void setNom(String nom) {
        this.nom = nom;
    }

    public String getPrenom() {
        return prenom;
    }

    public void setPrenom(String prenom) {
        this.prenom = prenom;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public int getPhone() {
        return phone;
    }

    public void setPhone(int phone) {
        this.phone = phone;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }


}
