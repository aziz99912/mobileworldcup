package isetb.tp6.worldcup;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;

import androidx.annotation.Nullable;

import java.util.ArrayList;

public class DatabaseHelper extends SQLiteOpenHelper {
    public static final  int DATABASE_VERSION = 1;
    public static final String DATABASE_NAME = "user_db";
    public static final String TABLE_USER ="user";
    public static final String COL_ID ="_id";
    public static final String COL_NAME ="name";
    public static final String COL_PHONNE ="phone";
    public static final String CREATE_USER_TABLE = "CREATE TABLE" + TABLE_USER + "(" + COL_ID + " INTEGER PRIMARY KEY AUTOINCRIMENT, "
            + COL_NAME + " TEXT NOT NULL, "
            + COL_PHONNE + " TEXT)";

    private SQLiteDatabase db ;

    public DatabaseHelper(@Nullable Context context) {
        super (context, DATABASE_NAME , null, DATABASE_VERSION);
    }




    @Override
    public void onCreate(SQLiteDatabase db) {
        db.execSQL(CREATE_USER_TABLE);

    }

    @Override
    public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
        db.execSQL("DROP TABLE IF exists " + TABLE_USER);
        onCreate(db);

    }

    public SQLiteDatabase open(){
        db = this.getWritableDatabase();
        return db ;
    }

    public void addUser (User u){
        open();
        ContentValues v = new ContentValues();
        v.put(COL_NAME,u.getNom());
        v.put(COL_PHONNE,u.getPhone());
        db.insert(TABLE_USER,null,v);
    }

    public void updateUser (User u) {
        open();
        ContentValues v = new ContentValues();
        v.put(COL_NAME,u.getNom());
        v.put(COL_PHONNE,u.getPhone());
        db.update(TABLE_USER,v,COL_ID+"=?",new String []{String.valueOf(u.getId())});
    }

    public void removeUser (int id) {
        open();
        db.delete(TABLE_USER, COL_ID+"=?", new String [] {String.valueOf(id)});
    }

    public ArrayList<User> getAllUser() {
        ArrayList<User> list =new ArrayList<User>();
        db=this.getReadableDatabase();
        Cursor c=db.query(TABLE_USER, null, null,null,null,null,null
                ,null);
        c.moveToFirst();
        do{
            User u=new User(c.getInt(0),c.getString(1),c.getString(2));
            list.add(u);
        }
        while (c.moveToNext());
        c.close();
        return list;


    }
    public int getUserCount() {
        db = this.getReadableDatabase();
        Cursor c = db.query(TABLE_USER, null, null, null, null, null, null
                , null);
        return c.getCount();
    }



    }
